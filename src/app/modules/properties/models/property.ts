import { Object } from 'parse';

import { PropertyData } from '../interfaces/property-data';

export class ParseProperty extends Object {}
Object.registerSubclass('Property', ParseProperty);

export class Property implements PropertyData {
    public id: any;
    public address: string;

    public constructor(entity: ParseProperty|PropertyData = null) {
        if (entity instanceof ParseProperty) {
            this.updateData(entity);
        } else if (entity) {
            this.id = entity.id;
            this.address = entity.address;
        }
    }

    public updateData(entity: ParseProperty) {
        this.address = entity.get('address');
        if (!entity.isNew()) {
            this.id = entity.id;
        }
    }
}
