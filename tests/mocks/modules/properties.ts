export const propertiesMock = [
    {address: "address 0, some where in space"},
    {address: "address 1, some where in space"},
    {address: "address 2, some where in space"},
];

export const personAccessMock = propertiesMock[0];

export const propertiesAPIResponseMock = propertiesMock;
