import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PersonsFormPage } from './persons-form.page';
import { PersonsFormComponent } from '../../components/persons-form/persons-form.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('PersonsFormPage', () => {
  let component: PersonsFormPage;
  let fixture: ComponentFixture<PersonsFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PersonsFormPage,
        PersonsFormComponent,
       ],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
        ]
    }).compileComponents();

    fixture = TestBed.createComponent(PersonsFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
