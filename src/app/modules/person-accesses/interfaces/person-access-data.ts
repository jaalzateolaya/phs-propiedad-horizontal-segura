import { PersonData } from '@modules/persons/interfaces/person-data';
import { PropertyData } from '@modules/properties/interfaces/property-data';
import { BackEndDataModel } from 'src/app/interfaces/back-end-data-model';

export interface PersonAccessData extends BackEndDataModel {
    person: PersonData;
    property: PropertyData;
}
