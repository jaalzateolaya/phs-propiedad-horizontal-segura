import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PersonsListPage } from './persons-list.page';
import { PersonsListComponent } from '../../components/persons-list/persons-list.component';
import { PersonComponent } from '../../components/person/person.component';

describe('PersonsListPage', () => {
  let component: PersonsListPage;
  let fixture: ComponentFixture<PersonsListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PersonsListPage,
        PersonsListComponent,
        PersonComponent,
      ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PersonsListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
