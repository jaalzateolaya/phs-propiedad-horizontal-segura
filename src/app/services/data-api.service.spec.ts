import { HttpErrorResponse } from '@angular/common/http';

import { FrontEndModel } from '@core/models/front-end-model';
import { BackEndDataModel } from '@core/interfaces/back-end-data-model';
import { API_BASE, API_HEADERS } from '@env/environment';
import { asyncData, asyncError } from '@tests/helpers';

import { DataAPIService } from './data-api.service';

describe('DataAPIService', () => {
    interface MockInterface {
        name: string;
        value: string;
    }

    class ConcreteDataAPIService extends DataAPIService<MockInterface> {
        public get className() {
            return className;
        }

        public handleRequestError(errorBody) {
            return Promise.reject(errorBody);
        }

        public beforeSave(data) {
            return Promise.resolve(data);
        }
    }

    let httpClientSpy: {
        get: jasmine.Spy,
        patch: jasmine.Spy,
        post: jasmine.Spy
    };
    let service: ConcreteDataAPIService;

    const className = 'SomeClass';
    const APIEndPoint = API_BASE + '/classes/' + className;
    const APIHeaders = API_HEADERS;
    const objectsMock = [
        {name: 'a', value: 'val'},
        {name: 'b', value: 'var'},
    ] as MockInterface[];

    beforeEach(() => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'patch', 'post']);
        service = new ConcreteDataAPIService(httpClientSpy as any);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return expected objects from server', async () => {
        let returnedObjects;

        httpClientSpy.get.and.returnValue(asyncData(objectsMock));

        returnedObjects = await service.getAll();

        expect(returnedObjects).toEqual(objectsMock);
        expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
        expect(httpClientSpy.get.calls.first().args[0]).toEqual(APIEndPoint);
        expect(httpClientSpy.get.calls.first().args[1]).toEqual({headers: APIHeaders});
    });

    it('should return a specific object when requested by id', async () => {
        let returnedObject;

        const id = 'asdf';
        const endpoint = APIEndPoint + '/' + id;

        httpClientSpy.get.and.returnValue(asyncData(objectsMock[0]));

        returnedObject = await service.getOne(id);

        expect(returnedObject).toEqual(objectsMock[0]);
        expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
        expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
        expect(httpClientSpy.get.calls.first().args[1]).toEqual({headers: APIHeaders});
    });

    it('should save using PATCH if the object exists', async () => {
        let returnedObject;

        const existentObject = Object.assign({
            id: '1a1a1a1a1a1a1a1a1a1a1a1a',
            exists: true,
        }, objectsMock[0]) as any;
        const endpoint = APIEndPoint + '/' + existentObject.id;

        httpClientSpy.patch.and.returnValue(asyncData(existentObject));

        returnedObject = await service.save(existentObject as FrontEndModel);

        expect(returnedObject).toEqual(existentObject);
        expect(httpClientSpy.post).toHaveBeenCalledTimes(0);
        expect(httpClientSpy.patch).toHaveBeenCalledTimes(1);
        expect(httpClientSpy.patch.calls.first().args[0]).toEqual(endpoint);
        expect(httpClientSpy.patch.calls.first().args[1]).toEqual(existentObject);
        expect(httpClientSpy.patch.calls.first().args[2]).toEqual({headers: APIHeaders});
    });

    it('should save using POST if the object does not exists', async () => {
        let returnedObject;

        const mock = Object.assign({isNew: true}, objectsMock[0]) as any;
        const endpoint = APIEndPoint;

        httpClientSpy.post.and.returnValue(asyncData(mock));

        returnedObject = await service.save(mock as FrontEndModel);

        expect(returnedObject).toEqual(mock);
        expect(httpClientSpy.patch).toHaveBeenCalledTimes(0);
        expect(httpClientSpy.post).toHaveBeenCalledTimes(1);
        expect(httpClientSpy.post.calls.first().args[0]).toEqual(endpoint);
        expect(httpClientSpy.post.calls.first().args[1]).toEqual(mock);
        expect(httpClientSpy.post.calls.first().args[2]).toEqual({headers: APIHeaders});
    });

    it('Should call `handleRequestError` on error from backend', async (done) => {
        const mock = Object.assign({isNew: true}, objectsMock[0]) as any;
        const endpoint = APIEndPoint;
        const errorResponse = new HttpErrorResponse({
            error: {code: 105, error: 'This is an error!!'},
            status: 500,
            statusText: 'Internal server error'
        });

        const errorHandlerSpy = spyOn(service, 'handleRequestError').and.callThrough();
        httpClientSpy.post.and.returnValue(asyncError(errorResponse));

        try {
            const ret = await service.save(mock as FrontEndModel);
            fail('Should throw an error');
        } catch (error) {
            expect(error).toBe(errorResponse);
            expect(errorHandlerSpy).toHaveBeenCalledTimes(1);
            done();
        }
    });
});
