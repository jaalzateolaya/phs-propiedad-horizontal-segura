import { Injectable } from '@angular/core';

export const SESSION_KEY = 'authentication.session';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor() { }

  /**
   * Return true when user is authenticated
   */
  public get isAuthenticated(): boolean {
    return localStorage.getItem(SESSION_KEY) !== null;
  }

  /**
   * Return the session token from local storage
   */

  public get sessionToken(): string {
    return this.getSession().sessionToken;
  }

  /**
   * Save the session information into local storage
   */
  public saveSession(sessionData): void {
    localStorage.setItem(SESSION_KEY, JSON.stringify(sessionData));
  }

  /**
   * Get the session information from local storage
   */
  public getSession(): any {
    return JSON.parse(localStorage.getItem(SESSION_KEY));
  }

  /**
   * Delete the session information stored in local storage
   */
  public cleanSession(): void {
    localStorage.removeItem(SESSION_KEY);
  }
}
