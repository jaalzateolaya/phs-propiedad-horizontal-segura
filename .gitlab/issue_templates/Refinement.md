<!---------------------------------------------------------------------------->
<!-- Context -->
Base Issue: #{N} {Issue description}



<!---------------------------------------------------------------------------->
# Tasks

* [ ] Contextualize the requirement.
* [ ] Relate/Link existent issues.
* [ ] List files related to the requirement.
* [ ] Review database entities related and describe how are they related.
* [ ] Review the existent code and analyze the algorithms.
* [ ] List external libraries required and describe how can be used.

<!---------------------------------------------------------------------------->
# Files related

* `app/file/1.ts`
* `app/file/2.ts`
* `app/file/3.ts`

<!---------------------------------------------------------------------------->
# Database entities

* `EntityName` Is where some registries are stored.
```ts
interface EntityName {
    aString: string;
    aNumber: number;
    aBoolean: boolean;
}
```

* `ComposistedEntity`: Some other related characteristic stored.
```ts
interface ComposistedEntity {
    anEntity: EntityName;
}
```

<!---------------------------------------------------------------------------->
# Algorithm analysis
<!--
* List of characteristics.
* Some relevant notes.

```
Code example
```

Use any format you prefer.
-->



<!---------------------------------------------------------------------------->
# External libraries required
<!--
List all libraries required and describe why are they required and why is is
preferred over alternatives.
-->

* Library A: Is needed to achieve X behavior. Peforms better than:
  * [ALT] Library B: This has Y problem and is not resolved yet.
  * [ALT] Library C: This has Y problem and is not resolved yet.
* Library D: bla, bla, bla. **No alternatives found**.

<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Type::Refinement" ~"Triaging:Stage::New"

