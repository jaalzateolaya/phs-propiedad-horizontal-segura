import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PropertiesFormPage } from './properties-form.page';
import { PropertyFormComponent } from '../../components/property-form/property-form.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('PropertiesFormPage', () => {
  let component: PropertiesFormPage;
  let fixture: ComponentFixture<PropertiesFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PropertiesFormPage,
        PropertyFormComponent,
      ],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PropertiesFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
