import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { Property } from '../../models/property';
import { PropertiesService } from '../../services/properties.service';
import { PropertiesFormPage } from '../properties-form/properties-form.page';
import { PropertyData } from '../../interfaces/property-data';

@Component({
  selector: 'app-properties-list-page',
  templateUrl: './properties-list.page.html',
  styleUrls: ['./properties-list.page.scss'],
})
export class PropertiesListPage implements OnInit {
   public properties: Property[];

  constructor(
    public modalController: ModalController,
    private propertyService: PropertiesService
  ) {}

  ngOnInit() {
    this.loadProperties();
  }

  public async loadProperties() {
    this.propertyService.loadProperties();
    this.properties = this.propertyService.properties;
    console.log('properties', this.properties);
  }

  public goToCreateProperty() {
    this.goToPropertyForm();
  }

  public onEditProperty(propertyData: PropertyData) {
    this.goToPropertyForm(propertyData);
  }

  public onRemoveProperty(propertyData: PropertyData) {
    const property = this.propertyService.createNew(propertyData);
    this.propertyService.remove(property);
  }

  public async goToPropertyForm(propertyData: PropertyData = null) {
    const modal = await this.modalController.create({
      component: PropertiesFormPage,
      componentProps: {propertyData},
    });
    await modal.present();
  }
}
