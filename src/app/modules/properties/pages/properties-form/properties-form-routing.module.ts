import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PropertiesFormPage } from './properties-form.page';

const routes: Routes = [
  {
    path: '',
    component: PropertiesFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PropertiesFormPageRoutingModule {}
