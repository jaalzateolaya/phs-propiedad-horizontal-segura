import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserData } from '../../interfaces/user-data';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.scss'],
})
export class UsersFormComponent implements OnInit {
  @Input()
  public userData: UserData;

  @Output()
  public submitForm: EventEmitter<{username: string}> = new EventEmitter();
  @Output()
  public cancelForm: EventEmitter<void> = new EventEmitter();

  private userForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.userForm = this.formBuilder.group({
      username: ['', [Validators.email, Validators.required]],
    });
   }

  ngOnInit() {
    if (this.userData) {
      this.userForm.setValue({
        username: this.userData.username,
      });
    }
  }

  public onSubmit(): void {
    const value = this.userForm.value;

    if (!this.userForm.valid) {
      // TODO: Define an error handling method.
      return;
    }

    if (this.userData) {
      value.id = this.userData.id;
    }

    this.submitForm.emit(value);
  }

  public onCancel(): void {
    this.cancelForm.emit();
  }
}
