import { Injectable } from '@angular/core';

import { NotAuthorizedError } from '@errors';
import { DataAPIService } from '@core/services/data-api.service';

import { PersonAccess } from '../models/person-access';

@Injectable({
  providedIn: 'root'
})
export class PersonAccessesService extends DataAPIService<PersonAccess> {
    public get className(): string {
        return 'PersonAccess';
    }

    public handleRequestError() {
        // TODO: Implement me
        return Promise.reject('false');
    }

    public beforeSave(data) {
        if (data.exists) {
            throw new NotAuthorizedError('Accesses cannot be updated');
        }

        return Promise.resolve(data);
    }
}
