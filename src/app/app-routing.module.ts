import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'users-list',
    loadChildren: () => import('./modules/users/pages/users-list/users-list.module').
    then( m => m.UsersListPageModule)
  },
  {
    path: 'properties-list',
    loadChildren: () => import('./modules/properties/pages/properties-list/properties-list.module').
    then( m => m.PropertiesListPageModule)
  },
  {
      path: 'persons-list',
    loadChildren: () => import('./modules/persons/pages/persons-list/persons-list.module').
    then( m => m.PersonsListPageModule)
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
