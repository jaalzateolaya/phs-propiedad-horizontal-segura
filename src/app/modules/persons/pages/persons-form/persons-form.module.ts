import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PersonsFormPageRoutingModule } from './persons-form-routing.module';

import { PersonsFormPage } from './persons-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PersonsFormPageRoutingModule
  ],
  declarations: [PersonsFormPage]
})
export class PersonsFormPageModule {}
