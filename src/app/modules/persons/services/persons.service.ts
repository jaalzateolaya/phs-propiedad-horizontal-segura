import { Injectable } from '@angular/core';
import { Query } from 'parse';

import { Person, ParsePerson } from '../models/person';
import { PersonData } from '../interfaces/person-data';

@Injectable({
  providedIn: 'root'
})
export class PersonsService {
  public residents: Person[];

  constructor() { }

  public async getResident(id): Promise<Person> {
    const parsePerson = await this.loadParseResident(id);
    return new Person(parsePerson);
  }

  public createNew(resident: PersonData): Person {
    return new Person(resident);
  }

  public async save(resident: Person) {
    if (resident.id) {
      const existantResident = this.residents.find((item) => item.id === resident.id);
      existantResident.email = resident.email;
      existantResident.firstName = resident.firstName;
      existantResident.lastName = resident.lastName;
    } else {
      resident.id = this.residents.length + 1;
      this.residents.push(resident);
    }
  }

  public loadResidents() {
    if (!this.residents) {
      this.residents = [];
    }
  }

  private async loadParseResident(id): Promise<ParsePerson> {
    const query = new Query(ParsePerson);
    const parsePerson = await query.get(id);

    return parsePerson;
  }

  public async remove(resident: Person) {
    const parsePerson = await this.loadParseResident(resident.id);
    await parsePerson.destroy();
  }


}
