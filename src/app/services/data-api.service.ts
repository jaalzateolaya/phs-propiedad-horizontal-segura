import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, tap, finalize } from 'rxjs/operators';

import { API_BASE, API_HEADERS } from '@env/environment';
import { FrontEndModel } from '@core/models/front-end-model';
import { BackEndDataModel } from '@core/interfaces/back-end-data-model';
import { AppError } from '@errors';

/**
 * Abstract API class to execute REST request over the main resource.
 */
export abstract class DataAPIService<T extends object> {
    /**
     * The class name is the actual resource we are quering.
     */
    public abstract get className(): string;

    /**
     * The API endpoint URL.
     */
    private get APIEndpoint(): string {
        return API_BASE + '/classes/' + this.className;
    }

    /**
     * The HTTPClient request options
     */
    private get requestOptions() {
        return {
            headers: API_HEADERS
        };
    }

    public constructor(protected httpClient: HttpClient) {}

    /**
     * Handles server errors.
     */
    public abstract handleRequestError(errorBody: any): Promise<any>;

    /**
     * Handles before-save event.
     *
     * @param data The data to be saved.
     */
    public abstract async beforeSave<U>(data: U): Promise<U | AppError>;

    /**
     * Loads one element from back end.
     *
     * @param id The id of the element.
     */
    public getOne(id: string): Promise<T> {
        const endpoint = this.APIEndpoint + '/' + id;

        return this.httpClient.get<T>(endpoint, this.requestOptions)
            .toPromise()
            .catch(this.handleRequestError);
    }

    /**
     * Loads all elements from the back end.
     */
    public getAll(): Promise<T[]> {
        return this.httpClient.get<T[]>(this.APIEndpoint, this.requestOptions)
            .toPromise()
            .catch(this.handleRequestError);
    }

    /**
     * Sents data to the back end to be saved.
     *
     * If data contains an id we use PATCH, else we use POST to save the data.
     *
     * @param data The data to be save into the server.
     */
    public async save(data: FrontEndModel): Promise<T> {
        await this.beforeSave(data);

        let method: string;
        let endpoint: string = this.APIEndpoint;

        const rawData = {};

        for (const key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }

            const value = data[key];

            rawData[key] = value instanceof FrontEndModel
                ? this.getRelationConfig(value)
                : value;
        }

        if (!data.exists) {
            method = 'post';
        } else {
            method = 'patch';
            endpoint = endpoint + '/' + data.id;
        }

        return this.httpClient[method](endpoint, rawData, this.requestOptions)
            .toPromise()
            .catch(this.handleRequestError);
    }

    /**
     * Creates a Parse-compatible relation object.
     *
     * @param object The object to be related.
     */
    private getRelationConfig(object: FrontEndModel) {
        return {
            __type: 'Pointer',
            className: object.constructor.name,
            objectId: object.id,
        };
    }

}
