import { PersonAccess } from '@modules/person-accesses/models/person-access';

import { personsMock } from './persons';
import { propertiesMock } from './properties';

export const personAccessesMock = [
    {
        person: personsMock[0],
        property: propertiesMock[0],
    },
    {
        person: personsMock[1],
        property: propertiesMock[1],
    },
    {
        person: personsMock[2],
        property: propertiesMock[2],
    },
] as any as PersonAccess[];

export const personAccessMock = personAccessesMock[0];

export const personAccessesAPIResponseMock = personAccessesMock;
