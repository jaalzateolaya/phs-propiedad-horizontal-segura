import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { PersonData } from '../../interfaces/person-data';
import { PersonsService } from '../../services/persons.service';

@Component({
  selector: 'app-persons-form-page',
  templateUrl: './persons-form.page.html',
  styleUrls: ['./persons-form.page.scss'],
})
export class PersonsFormPage implements OnInit {
  @Input()
  public resident: PersonData;

  public constructor(
    public personsService: PersonsService,
    public modalController: ModalController
  ) {}

  public ngOnInit() { }

  public onSubmit(residentForm: PersonData) {
    this.save(residentForm);
  }

  public async save(residentForm: PersonData) {
      const resident = this.personsService.createNew(residentForm);
      await this.personsService.save(resident);

      this.onAfterSaveData();
  }

  public onAfterSaveData() {
    this.modalController.dismiss();
  }

  public onCancelForm() {
    this.modalController.dismiss();
  }
}

