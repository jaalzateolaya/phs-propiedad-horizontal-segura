import { TestBed, async } from '@angular/core/testing';

import { AuthenticationService } from './authentication.service';

describe('AuthenticationService', () => {
  let service: AuthenticationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = new AuthenticationService();
    localStorage.clear();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve session information from local storage', () => {
    const loginMock = {
      username: 'cooldude6@cooldomain.com',
      phone: '415-392-0202',
      objectId: 'g7y9tkhB7O',
      sessionToken: 'r:pnktnjyb996sj4p156gjtp4im',
    };
    service.saveSession(loginMock);

    const session = service.getSession();

    expect(session).toEqual(loginMock);
  });

  it('should return false when session is not started', () => {
    const isSessionStarted = service.isAuthenticated;

    expect(isSessionStarted).toBeFalse();
  });

  it('should return true when session is not started', () => {
    const loginMock = {
      username: 'cooldude6@cooldomain.com',
      phone: '415-392-0202',
      objectId: 'g7y9tkhB7O',
      sessionToken: 'r:pnktnjyb996sj4p156gjtp4im',
    };
    service.saveSession(loginMock);

    const isSessionStarted = service.isAuthenticated;

    expect(isSessionStarted).toBeTrue();
  });
});
