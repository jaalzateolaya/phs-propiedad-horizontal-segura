import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { User } from '../../models/user';
import { UsersService } from '../../services/users.service';
import { UsersFormPage } from '../users-form/users-form.page';
import { UserData } from '../../interfaces/user-data';

@Component({
  selector: 'app-users-list-page',
  templateUrl: './users-list.page.html',
  styleUrls: ['./users-list.page.scss'],
})
export class UsersListPage implements OnInit {
  public users: User[];

  public constructor(
    private usersService: UsersService,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.loadUsers();
  }

  public onEditUser(userData: UserData) {
    this.goToUserForm(userData);
  }

  public async goToUserForm(userData: UserData = null) {
    const modal = await this.modalController.create({
      component: UsersFormPage,
      componentProps: {userData},
    });

    await modal.present();
  }

  public loadUsers() {
    this.usersService.loadUsers();
    this.users = this.usersService.users;
  }
}
