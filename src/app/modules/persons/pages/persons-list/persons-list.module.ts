import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PersonsListPageRoutingModule } from './persons-list-routing.module';

import { PersonsListPage } from './persons-list.page';
import { PersonsFormPage } from '../persons-form/persons-form.page';
import { PersonComponent } from '../../components/person/person.component';
import { PersonsListComponent } from '../../components/persons-list/persons-list.component';
import { PersonsFormComponent } from '../../components/persons-form/persons-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PersonsListPageRoutingModule
  ],
  declarations: [
                PersonsListPage,
                PersonsFormPage,
                PersonComponent,
                PersonsListComponent,
                PersonsFormComponent
  ],
  entryComponents: [PersonsFormPage]
})
export class PersonsListPageModule {}
