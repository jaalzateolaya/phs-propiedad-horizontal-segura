<!---------------------------------------------------------------------------->
<!-- Describe the new feature -->
~"Type::User Requirement": #{N of ~"Type::User Requirement"}
/relate #{N of ~"Type::User Requirement"}



<!---------------------------------------------------------------------------->
# Tasks
<!--
Define one by one the tasks to finish (implement) this issue.
Format should be:

* [ ] Task name `estimated time expressed in min or h`

If the task is longer than a day, it should be splitted into different
*issues**.
-->

* [ ] Task 1 `1h`
* [ ] Task 2 `30min`
* [ ] Task 3 `1.5h`

<!---------------------------------------------------------------------------->
# Definition of Done
<!--
Which criteria should the implementation have to be considered done?

See: https://www.agilealliance.org/glossary/definition-of-done/
-->

* [ ] First criteria.
* [ ] Second criteria.

<!---------------------------------------------------------------------------->
# Additional info

<!--........................................................................-->
## Design Proposal



<!--........................................................................-->
## Design Notes



<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Type::Feature" ~"Triaging:Stage::New"

