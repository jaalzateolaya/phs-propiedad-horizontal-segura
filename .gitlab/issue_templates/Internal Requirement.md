<!---------------------------------------------------------------------------->
<!-- Describe the requirement -->



<!---------------------------------------------------------------------------->
# Definition of Done
<!--
Which criteria should the implementation have to be considered done?

See: https://www.agilealliance.org/glossary/definition-of-done/
-->

* [ ] First criteria.
* [ ] Second criteria.

<!---------------------------------------------------------------------------->
# Additional info

<!--........................................................................-->
## Design Proposal



<!--........................................................................-->
## Design Notes



<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Type::Internal Requirement" ~"Triaging:Stage::New"

