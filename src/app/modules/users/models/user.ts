import * as Parse from 'parse';

import { UserData } from '../interfaces/user-data';

export class User implements UserData {
    public id: any;
    public username: string;
    public password: string;

    public constructor(entity: Parse.User | {username: string} = null) {
        if (entity instanceof Parse.User) {
            this.updateData(entity);
        } else if (entity) {
            this.username = entity.username;
        }
    }

    public updateData(entity: Parse.User) {
        this.username = entity.get('username');
        this.password = null;

        if (!entity.isNew()) {
            this.id = entity.id;
        }
    }
}
